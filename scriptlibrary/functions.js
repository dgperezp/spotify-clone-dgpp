//SEARCH VARS
var currentIndex = 1;
var maximumItems = 50;
var order = ["relevance", "published", "viewCount", "rating"];
var player;
var playerIframe;
var timer;



function debug(log) {
    if (window.console != undefined) {
        console.log(log);
    }
}


function testSearch(query){
	var testQuery = "https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults="+maximumItems+"&pageToken="+" "+"&order="+order[0]+"&type=/music/&key=AIzaSyBEEvY7m7ic7nUXUMi76TtMwpGcYWuOMhg&q="+query+"&callback=?";
	debug(testQuery);
	
	$.ajax({
		url: testQuery,
		dataType: 'jsonp',
		crossDomain: true,
		beforeSend: function(request) {
		},
		success: function(data, textStatus, request) {
		   debug(data);
		   
		   onResult(data);
		   tempQuery = query;
		   findSimilar(query);
		   getArtist(query);
		   
		},
		error: function(request, status, error) {
			alert(error)
		}
	});
	
	
}